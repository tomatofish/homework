export const data = [
  {
    "id": "sale",
    "type": "product_list",
    "attributes": {
      "name": "區塊一",
      "custom": [
        {
          "id": "1",
          "type": "product",
          "attributes": {
            "productable_id": "1",
            "name": "商品一",
            "slogan": "文字文字文字文字文字文字文字文字文字文字文字文字文字文字文字文字文字",
            "shop_cover_image_url": "https://picsum.photos/375/210",
            "count": 100,
            "currency": "TWD",
            "list_price": "2000",
            "sale_price": "1500",
            "tags": [
              "商品一",
            ],
            "average_rating": 4.8,
            "rating_count": 100
          }
        },
        {
          "id": "2",
          "type": "product",
          "attributes": {
            "productable_id": "2",
            "name": "商品二",
            "slogan": "文字文字文字文字文字",
            "shop_cover_image_url": "https://picsum.photos/375/210",
            "count": 150,
            "list_price": "4900",
            "sale_price": "2450",
            "tags": ["商品二"],
            "average_rating": 4.51,
            "rating_count": 50
          }
        },
        {
          "id": "3",
          "type": "product",
          "attributes": {
            "productable_id": "3",
            "name": "商品三",
            "slogan": "文字文字文字文字文字文字文字文字文字文字",
            "shop_cover_image_url": "https://picsum.photos/375/210",
            "count": 300,
            "list_price": "420",
            "sale_price": "330",
            "tags": ["商品三"],
            "average_rating": 4.46,
            "rating_count": 10000
          }
        },
        {
          "id": "4",
          "type": "product",
          "attributes": {
            "productable_id": "4",
            "name": "商品四",
            "slogan": "文字文字文字文字文字文字文字文字文字文字文字文字文字文字文字文字文字文字文字文字",
            "shop_cover_image_url": "https://picsum.photos/375/210",
            "count": 5900,
            "list_price": "449",
            "sale_price": "390",
            "tags": ["商品四"],
            "average_rating": 4.2,
            "rating_count": 99
          }
        }
      ]
    }
  },
  {
    "id": "test",
    "type": "product_best",
    "attributes": {
      "name": "test",
      "custom": [
        {
          "id": "2",
          "type": "best_product",
          "attributes": {
            "productable_id": "2",
            "name": "商品",
            "slogan": "文字文字文字文字文字文字文字文字文字文字文字文字文字",
            "shop_cover_image_url": "https://picsum.photos/375/210",
            "count": 150,
            "list_price": "4900",
            "sale_price": "2450",
            "tags": ["商品"],
            "average_rating": 3.5,
            "rating_count": 50
          }
        },
      ]
    }
  }
]